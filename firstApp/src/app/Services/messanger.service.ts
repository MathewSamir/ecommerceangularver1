import { Injectable, TemplateRef } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MessangerService {
  subject = new Subject();
  constructor() {}

  sendMsg(product) {
    this.subject.next(product); // trigger event
  }
  getMsg() {
    return this.subject.asObservable();
  }

  sendUserName(user) {
    this.subject.next(user);
  }
  getUserName() {
    return this.subject.asObservable();
  }

  fireLogout() {
    this.subject.next('');
  }

  logoutEvent() {
    return this.subject.asObservable();
  }

  sub2 = new Subject();
  sendModalContent(content: TemplateRef<any>) {
    this.sub2.next(content);
  }
  getModalContent() {
    return this.sub2.asObservable();
  }


  sendToken(token)
  {
    this.subject.next(token)
  }
  getToken()
  {
    return this.subject.asObservable();
  }
}
