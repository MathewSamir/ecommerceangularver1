import { Injectable } from '@angular/core';
import{HttpClient} from '@angular/common/http';
import {orderUrl} from 'src/app/config/api'
//import{ordersForSpecificUserUrl} from 'src/app/config/api';
import { from } from 'rxjs';
import { getLocaleExtraDayPeriods } from '@angular/common';

@Injectable({
  providedIn: 'root'
})

export class OrdersSpecificUserService {
//id="5ea2ebd283dd7903900b92c6";
 //ordersForSpecificUserUrl =`http://localhost:3000/api/orders/user`
 orders={};
 user;
  constructor(private myClient:HttpClient) {}
    getOrders(id){
      return this.myClient.get(`http://localhost:3000/api/orders/user/${id}`);
    }
    getUserInfo(id){
      return this.myClient.get(`http://localhost:3000/api/users/${id}`);
    }
    editUserInfo(id,data){
      return this.myClient.put(`http://localhost:3000/api/users/${id}`,data);
    }
  
   addOrder(cartID)
   {
       return this.myClient.post(orderUrl, { "cart":cartID });
   } 


}
