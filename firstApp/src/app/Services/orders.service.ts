import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  constructor(private myClient: HttpClient) { }
  
  getOrders() {
    return this.myClient.get(`http://localhost:3000/api/orders`);
  }
  editStatus(orderId,newStatus){
    return this.myClient.patch(`http://localhost:3000/api/orders/${orderId}`,newStatus);
  }
}
