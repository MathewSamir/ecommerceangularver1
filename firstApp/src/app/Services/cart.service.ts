import { Injectable } from '@angular/core';
import { Product } from '../Model/product.Model';
import {CartItem} from '../Model/cartItem.Model';
import { cartUrl } from 'src/app/config/api';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { async } from '@angular/core/testing';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(private _http:HttpClient) { }
  

      
    getCartItems(userId){
      let cartItem:CartItem = null; 
    
     return this._http.get<CartItem>(cartUrl+"/user/"+userId)     
   
  }


  getCartQuantity(userId)
  {
    console.log(`id is ${userId}`);
    return this._http.get<number>(cartUrl+"/user/quantity/"+userId) 
  }


 


   addProductToCart(productID: string,UserID:string) {

     return this._http.post(cartUrl, { "user":UserID,"product":productID });
    }

  
   



 
 

  }