import { Injectable, SecurityContext } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product } from '../Model/product.Model';
import { productsUrl } from 'src/app/config/api';
import { DomSanitizer } from '@angular/platform-browser'
import {ImageUrl} from 'src/app/config/api'

@Injectable({
  providedIn: 'root',
})
export class ProductDataService {
  //  apiURL = "http://localhost:3000/api/products"
  constructor(private _http: HttpClient,private sanitization:DomSanitizer ) {}

  getProducts() {
    return this._http.get<Product[]>(productsUrl)
  
    // return this._http.get(productsUrl);
  }




  getProductById(id) {
    return this._http.get(`${productsUrl}/${id}`);
  }

  updateImagesPathForProduct(products)
  {
      products.forEach(element => {
     var actualPath = element.imagePath.slice(8);    
     var currentPath= ImageUrl + actualPath;   
        element.imagePath = this.sanitization.sanitize(SecurityContext.RESOURCE_URL, this.sanitization.bypassSecurityTrustResourceUrl(currentPath));
     
    // this.sanitization.bypassSecurityTrustUrl(`${this.apiUrl}/${element.imagePath}`);
   });
  }
  

 
  updateImagePathForProduct(product)
  {
    
     var actualPath = product.imagePath.slice(8);    
     var currentPath= ImageUrl + actualPath;   
        product.imagePath = this.sanitization.sanitize(SecurityContext.RESOURCE_URL, this.sanitization.bypassSecurityTrustResourceUrl(currentPath));
     
    // this.sanitization.bypassSecurityTrustUrl(`${this.apiUrl}/${element.imagePath}`);
   
  }
 

  updateProduct(product): any {
    let formData = new FormData();
    formData.append('title', product.title);
    formData.append('description', product.description);
    formData.append('quantity', product.quantity);
    formData.append('imagePath', product.file, product.file.name);
    formData.append('price', product.price);

    console.log(product._id);
    // let prod = {
    //   title: product.title,
    //   description: product.description,
    //   price: product.price,
    //   quantity: product.quantity,
    //   imagePath: product.imagePath,
    // };
    return this._http.put(`${productsUrl}/${product._id}`, formData);
  }
 
  addProduct(product)
  {
    let formData = new FormData();
  

    formData.append('title', product.title);
    formData.append('description', product.description);
    formData.append('price', product.price);
  
    formData.append('imagePath', product.file ,product.file.name )

    return this._http.post(`${productsUrl}`, formData);
  } 



}
