import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductDeatilsService {

  constructor(private myProduct:HttpClient) { }

  getProdctInfo(id){
    return this.myProduct.get(`http://localhost:3000/api/products/${id}`);
  }
}
