import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { JsonPipe } from '@angular/common';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  baseUrl = 'http://localhost:3000/api/auth/me';
  constructor(private authClient: HttpClient) {}

  authorizeUser(token): any {
    return this.authClient.get(this.baseUrl, { headers: token });
  }
}
