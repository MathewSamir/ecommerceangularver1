import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class RegisterService {
  baseUrl = 'http://localhost:3000/api/users';

  constructor(private registerClient: HttpClient) {}

  createUser(userObj): any {
    let formData = new FormData();
    // console.log(JSON.stringify(userObj.name))
    // console.log(userObj.file.name)

    formData.append('name', userObj.name);
    formData.append('email', userObj.email);
    formData.append('password', userObj.password);
    formData.append('gender', userObj.gender);
    // formData.append(
    //   'data',
    //   JSON.stringify({
    //     name: userObj.name,
    //     email: userObj.email,
    //     password: userObj.password,
    //    gender:userObj.gender,
    //    })
    // );
    formData.append('userImage', userObj.file, userObj.file.name);
    // console.log(userObj)
    console.log(formData);
    return this.registerClient.post(this.baseUrl, formData);
    // return this.registerClient.post(this.baseUrl, userObj);
  }

  editUser(userObj,id): any {
    let formData = new FormData();
    formData.append('name', userObj.name);
    formData.append('email', userObj.email);
    formData.append('password', userObj.password);
    formData.append('userImage', userObj.file ,userObj.file.name )
    console.log(formData); 
    return this.registerClient.put(this.baseUrl+"/"+id, formData);
  }











}
