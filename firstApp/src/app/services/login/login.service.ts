import { Injectable, SecurityContext } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser'
import {ImageUrl} from 'src/app/config/api'

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  baseUrl: string = 'http://localhost:3000/api/auth';

  constructor(private myClient: HttpClient,private sanitization:DomSanitizer) {}

  addUser(userObj): any {
    return this.myClient.post(this.baseUrl, userObj);
  }

  updateImagePathForUserProfile(user)
  {
      //products.forEach(element => {


     var actualPath = user.userImage.slice(8);    
     var currentPath= ImageUrl + actualPath;   
      user.userImage = this.sanitization.sanitize(SecurityContext.RESOURCE_URL, this.sanitization.bypassSecurityTrustResourceUrl(currentPath));
     
    // this.sanitization.bypassSecurityTrustUrl(`${this.apiUrl}/${element.imagePath}`);
  // });
  }



}
