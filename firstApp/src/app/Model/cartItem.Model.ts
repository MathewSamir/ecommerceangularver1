import { Product } from './product.Model';

export class CartItem {
    _id:string;
    user:string;
    products:[Product]
    // productName:string;
    //products:[Product];
    status:string;


  constructor(__id,_products: [Product],_user:string) {
   
      this._id = __id;
      this.products= _products;
      this.user = _user;

  
   }

}
