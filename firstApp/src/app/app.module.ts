import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { MatRadioModule } from '@angular/material/radio';
import { MatInputModule } from '@angular/material/input';

import { AppComponent } from './app.component';
import { ProductsComponent } from './components/products/products.component';
import { ProductItemComponent } from './components/products/product-item/product-item.component';
import { PormotionComponent } from './components/pormotion/pormotion.component';
import { AboutComponent } from './components/about/about.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { CartComponent } from './components/cart/cart.component';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ErrorComponent } from './components/error/error.component';
import { ProductDataService } from './services/product.service';
import { LoginService } from './services/login/login.service';
import { RegisterService } from './services/register/register.service';
import { AuthService } from './services/auth/auth.service';
import { ProfileComponent } from './components/profile/profile.component';
import { LogoutComponent } from './components/logout/logout/logout.component';
import { EditProfileComponent } from './components/edit-profile/edit-profile.component';
import { CartItemComponent } from './components/cart/cart-item/cart-item/cart-item.component';
import { OrdersAdminComponent } from './components/orders-admin/orders-admin.component';
import { AdminProductsComponent } from './components/admin-products/admin-products.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { from } from 'rxjs';
import { CreateProductComponent } from './components/create-product/create-product.component';
import { MiniCartComponent } from './components/mini-cart/mini-cart.component';
import { FooterComponent } from './components/footer/footer.component';
import { FashionComponent } from './components/fashion/fashion.component';
import { FeaturesComponent } from './components/features/features.component';
import { DetailsComponent } from './components/details/details.component';
const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'editProfile', component: EditProfileComponent },
  { path: 'adminOrders', component: OrdersAdminComponent },
  { path: 'adminCreateProducts', component: CreateProductComponent },


  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'cart', component: CartComponent },
  { path: 'products', component: ProductsComponent },
  // { path: 'adminProducts', component: AdminProductsComponent },
  { path: 'adminProducts/:id', component: ProductDetailsComponent },
  { path:'details/:id',component:DetailsComponent},
  { path: '**', component: ErrorComponent },
];

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    ProductItemComponent,
    PormotionComponent,
    AboutComponent,
    LoginComponent,
    RegisterComponent,
    CartComponent,
    HomeComponent,
    ErrorComponent,
    ProfileComponent,
    LogoutComponent,
    CartItemComponent,
    EditProfileComponent,
    AdminProductsComponent,
    ProductDetailsComponent,
    OrdersAdminComponent,
    CreateProductComponent,
    MiniCartComponent,
    FooterComponent,
    FashionComponent,
    FeaturesComponent,
    DetailsComponent,
  ],

  imports: [
    NgbModule,
    BrowserAnimationsModule,
    MatRadioModule,
    MatInputModule,
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule,
    HttpClientModule,
    Ng2SearchPipeModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
  ],
  providers: [ProductDataService, LoginService, RegisterService, AuthService],

  bootstrap: [AppComponent],
})
export class AppModule {}
// export function tokenGetter() {
//   return localStorage.getItem('token');
// }
