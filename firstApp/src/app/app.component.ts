import {
  Component,
  ComponentRef,
  ViewChild,
  AfterViewInit,
  OnInit,
  Input,
} from '@angular/core';
import { RegisterComponent } from './components/register/register.component';
import { AuthService } from './services/auth/auth.service';
import { MessangerService } from './Services/messanger.service';
import { TranslateService } from '@ngx-translate/core';
import { Product } from 'src/app/Model/product.Model';
import { CartService } from 'src/app/Services/cart.service';
import { LoginService } from './services/login/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  name;
  role;
  userID;
  isNormalUser = false;
  IsAdmin = false;
  IsName = false;
  @Input() numberofproducts = 0;
  @Input() userImage;
  constructor(
    private msg: MessangerService,
    private authSerivce: AuthService,
    private cartService: CartService,
    public translate: TranslateService,
    private userProfile: LoginService
  ) {
    console.log(localStorage.getItem('token'));
    translate.addLangs(['en', 'ar']);
    translate.setDefaultLang('en');
    const browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/en | ar/) ? browserLang : 'en');

    this.msg.logoutEvent().subscribe(
      () => {
        console.log('here our subsc');
        this.name = undefined;
        this.IsName = false;
        this.IsAdmin = false;
        this.isNormalUser = false;
      },
      (err) => {
        console.log('didnot subsc');
      }
    );
  }

  ngOnInit(): void {
    console.log('hereeee main appp');

    this.msg.getUserName().subscribe(
      (userName) => {
        if (userName) {
          let stringifiedUser = JSON.stringify(userName);
          let userObject = JSON.parse(stringifiedUser);

          this.IsName = true;
          this.name = userObject.name;

          this.validateRole(userObject);
        }
      },
      (err) => {
        console.log(err);
      }
    );

    let token = localStorage.getItem('token');
    /// authenicate user and set some data
    this.authSerivce.authorizeUser({ token: token }).subscribe(
      (s) => {
        this.userID = s._id;
        this.name = s.name;
        this.IsName = true;
        this.userProfile.updateImagePathForUserProfile(s);
        this.userImage = s.userImage;
        if (s.role === 'Normal User') {
          this.isNormalUser = true;
          this.cartService
            .getCartQuantity(this.userID)
            .subscribe((res: any) => {
              console.log('iam here on init s');

              console.log(`res is ${res.quantity}`);

              this.numberofproducts = res.quantity;
            });
        } else if (s.role === 'Admin') {
          this.IsAdmin = true;
          console.log('he is admin');
        }

        this.validateRole(s);
        this.handleSubscription();
        console.log('user idddddddddddddddddddddddd', s._id);
        console.log('heeeeeeeeeeeeeeeeeeeeeeeeeeereee', s.name);
      },
      (err) => {
        console.log(err);
      }
    );
  }

  handleSubscription() {
    this.msg.getMsg().subscribe((product: Product) => {
      console.log('iam here on handle subscrib');
      this.cartService.getCartQuantity(this.userID).subscribe((res: any) => {
        console.log(`res is ${res.quantity}`);
        this.numberofproducts = res.quantity;
      });
      this.numberofproducts++;
    });
  }

  validateRole(userObject) {
    if (userObject.role === 'Normal User') {
      this.isNormalUser = true;
    } else if (userObject.role === 'Admin') {
      this.IsAdmin = true;
    }
  }
}
