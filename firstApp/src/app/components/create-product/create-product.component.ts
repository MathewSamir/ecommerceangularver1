import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Product } from 'src/app/Model/product.Model';
import { ProductDataService } from 'src/app/Services/product.service';


@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})
export class CreateProductComponent implements OnInit {
  selectedFile: File;
  registerForm: FormGroup;
  submitted = false;
  product:any = null

  constructor(private formBuilder:FormBuilder,private productService:ProductDataService) { }

  ngOnInit(): void {
    // this.registerForm = this.formBuilder.group({
    //   title: ['', Validators.required],
    //   firstName: ['', Validators.required],
    //   lastName: ['', Validators.required],
    //   email: ['', [Validators.required, Validators.email]],
    //   password: ['', [Validators.required, Validators.minLength(6)]],
    //   confirmPassword: ['', Validators.required]


     this.registerForm = this.formBuilder.group({
      title: ['', Validators.required],
         description: ['', Validators.required],    
         price: ['', Validators.required],
         imagePath: ['', [Validators.required]],
  });
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
      this.submitted = true;

      // stop here if form is invalid
      if (this.registerForm.invalid) {
          return;
      }
          else
        {
          console.log(this.registerForm)

          this.product = {
            title :JSON.stringify(this.registerForm.value.title),
            description:JSON.stringify(this.registerForm.value.description),
            price:JSON.stringify(this.registerForm.value.price),
            file:this.selectedFile, 
          }

           this.productService.addProduct(this.product).subscribe((res)=>{
             this.onReset();
            });
         
        }
      // display form values on success
      // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value, null, 4));
  }

  onFileSelected(evetObj) {
    this.selectedFile = evetObj.target.files[0];
    //console.log(this.selectedFile);
  }


  onReset() {
    this.submitted = false;
    this.registerForm.reset();
}
}

