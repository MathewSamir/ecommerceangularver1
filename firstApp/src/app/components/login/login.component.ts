import {
  Component,
  OnInit,
  ViewEncapsulation,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
  AfterViewChecked,
  AfterViewInit,
  TemplateRef,
} from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LoginService } from 'src/app/services/login/login.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MessangerService } from 'src/app/Services/messanger.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  encapsulation: ViewEncapsulation.Emulated,
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  img = `assets/images/newsletter-popup.jpg`;
  closeResult: string;
  email;
  isValidEmail: boolean;
  isWrongEmailOrPass: boolean;

  isValidPass: boolean;
  password;
  userName;

  @Output() myEvent = new EventEmitter();

  constructor(
    private modalService: NgbModal,
    private loginService: LoginService,
    private authSerivce: AuthService,
    private router: Router,
    private msg: MessangerService
  ) {}

  // ngAfterViewInit(): void {
  //   // throw new Error("Method not implemented.");
  //   this.openLg(this.content);

  // }
  // ngAfterViewChecked(): void {
  //   this.openLg(this.content);
  // }

  ngOnInit(): void {}
  @ViewChild('content', { static: true }) content: ElementRef;
  /* #region  ui funs */
  openBackDropCustomClass(content) {
    console.log('first content', content);
    this.modalService.open(content, { backdropClass: 'light-blue-backdrop' });
  }

  openWindowCustomClass(content) {
    console.log('second content', content);
    this.modalService.open(content, { windowClass: 'dark-modal' });
  }

  openSm(content) {
    console.log('third content', content);

    this.modalService.open(content, { size: 'sm' });
  }

  openLg(content: TemplateRef<any>) {
    // console.log('fourth');
    console.log(content);

    this.msg.sendModalContent(content);
    this.modalService.open(content, { size: 'lg' }).result.then((result) => {
      this.closeResult = `closed with ${result}`;
    });
  }

  openXl(content) {
    console.log('fifth content');
    this.modalService.open(content, { size: 'xl' });
  }

  openVerticallyCentered(content) {
    console.log('sixth content');
    this.modalService.open(content, { centered: true });
  }

  openScrollableContent(longContent) {
    console.log('seventh content');
    this.modalService.open(longContent, { scrollable: true });
  }

  /* #endregion */

  login(): any {
    this.isValidEmail = false;
    this.isWrongEmailOrPass = false;
    this.isValidPass = false;
    let userData = {
      email: this.email,
      password: this.password,
    };
    this.loginService.addUser(userData).subscribe(
      (res) => {
        localStorage.setItem('token', res.token);
        this.modalService.dismissAll();
        console.log(this.userName);
        let token = localStorage.getItem('token');
        console.log(typeof token);
        this.authSerivce.authorizeUser({ token: token }).subscribe(
          (s) => {
            this.userName = s.name;
             // this.loginService.updateImagePathForUserProfile(s);
             this.router.navigate(['products']);

            console.log(s.userImage);
            this.myEvent.emit(this.userName);
            // this.msg.sendUserName(this.userName);
            this.msg.sendUserName(s);
            console.log(this.userName);
            console.log('user idddddddddddddddddddddddd', s._id);
            console.log('heeeeeeeeeeeeeeeeeeeeeeeeeeereee', s);
          },
          (err) => {
            console.log(err);
          }
        );
      },
      (err) => {
        let emailError = 'email" is required';
        console.log(err);
        let passError = 'password" is required';
        // let passError2 = 'password" is not allowed to be empty';
        if (err.error === '"' + emailError) {
          console.log('match', err);
          this.isValidEmail = true;
        } else if (err.error === '"' + passError) {
          this.isValidPass = true;
        } else if (err.error === 'Invalid email or password') {
          this.isWrongEmailOrPass = true;
        }
      }
    );
  }

  // loginForm = new FormGroup({
  //   // name: new FormControl(' ', Validators.required),
  //   email: new FormControl(' ', Validators.email),
  //   password: new FormControl(' ', [
  //     Validators.required,
  //     Validators.minLength(7),
  //     Validators.maxLength(20),
  //   ]),
  //   phone: new FormControl('', Validators.minLength(11)),
  // });
}
