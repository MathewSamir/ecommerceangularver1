import { Component, OnInit, SecurityContext } from '@angular/core';
import { Product } from 'src/app/Model/product.Model';
import {CartItem} from 'src/app/Model/cartItem.Model';
import { CartService } from 'src/app/Services/cart.service';
import {MessangerService} from 'src/app/Services/messanger.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ProductDataService } from 'src/app/Services/product.service';




import { OrdersSpecificUserService } from 'src/app/Services/orders-specific-user.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  currentCart : CartItem;
  cartItems = undefined;
  userid;
  

  cartTotal = 0

  constructor(private msg: MessangerService,
    private cartService: CartService,private orderService:OrdersSpecificUserService,private authService:AuthService,
    private productDataService :ProductDataService 
    ) { }

  ngOnInit(): void {
    let token = localStorage.getItem('token');
    this.authService.authorizeUser({ token: token }).subscribe(
      (s) => {
        this.userid=s._id
        this.handleSubscription();
        this.loadCartItems();
      },
      (err) => {
        console.log(err);
      })
    
   
  }

  handleSubscription() {
    this.msg.getMsg().subscribe((product: Product) => {
      this.loadCartItems();
    })
  }

  loadCartItems() {
    this.cartService.getCartItems(this.userid).subscribe((item: CartItem) => {
      if(item != null)
      {
         this.currentCart = item; 
         if(item.products)
           {
             this.cartItems = item.products;
             this.productDataService.updateImagesPathForProduct(this.cartItems);

            //  this.cartItems.forEach(element => {
            //   var actualPath = element.imagePath.slice(8);
            //   var currentPath= productImageUrl + actualPath;
            //      element.imagePath = this.sanitization.sanitize(SecurityContext.RESOURCE_URL, this.sanitization.bypassSecurityTrustResourceUrl(currentPath));
            //  });
           
          }
        this.calcCartTotal();
      }
      else
      {
        this.cartItems = undefined;
      }
    })
  }
  calcCartTotal() {
    this.cartTotal = 0
       this.cartItems.forEach(item => {
        this.cartTotal += (item.quantity * item.price)
     })
  }

  handleAddOrder()
  {
    this.orderService.addOrder(this.currentCart._id).subscribe(() => {
      this.handleSubscription();
      this.loadCartItems();
      var product = Product;
      this.msg.sendMsg(product);
  })

  }



}
