import { Component, OnInit, Output, Input } from '@angular/core';
 import { OrdersSpecificUserService } from 'src/app/Services/orders-specific-user.service';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';
import {LoginService} from 'src/app/Services/login/login.service'
import { RegisterService } from 'src/app/services/register/register.service';


import { EventEmitter } from 'events';
import { kMaxLength } from 'buffer';
import { fileURLToPath } from 'url';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {
user;
userid;
selectedFile: File ;
@Input() userImage;

public editProfileForm =new FormGroup({
  name: new FormControl('',[Validators.minLength(7),Validators.maxLength(255)]),
  // phone: new FormControl('',Validators.minLength(8)),
  password : new FormControl('',[Validators.minLength(5),Validators.maxLength(1024)]),
  email : new FormControl('',Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/))
});
get userNameStatus(){return this.editProfileForm.controls.name.valid}
get phoneStatus(){return this.editProfileForm.controls.phone.valid}
get passwordStatus(){return this.editProfileForm.controls.password.valid}
get emailStatus(){return this.editProfileForm.controls.email.valid}

//@Output() myEvent = new EventEmitter();
addEdit(){
  
  console.log(this.editProfileForm.valid)
   if(this.editProfileForm.valid){
   console.log("ahm 7aga",this.editProfileForm.controls.name.value);

    if(this.editProfileForm.controls.name.value!==""){
      console.log("before name edit",this.editProfileForm.controls.name.value);
    this.user.name = this.editProfileForm.controls.name.value
    console.log("after name edit",this.user.name);
    }
     if(this.editProfileForm.controls.password.value!=null)
     {console.log("before password edit",this.user.password );
       this.user.password = this.editProfileForm.controls.password.value
       console.log("after password edit",this.user.password );
      }

    if(this.editProfileForm.controls.email.value!=null){
      console.log("before email edit",this.user.email);
      this.user.email = this.editProfileForm.controls.email.value
      console.log("after email edit",this.user.email );
    }
    
    let userObj = {
      name: this.user.name,
      email: this.user.email,
      password: this.user.password,
      file: this.selectedFile,
    };

    console.log("selected file is  in updates",this.selectedFile)

    
     console.log(`user is ${userObj}`)
    this.service.editUser(userObj,this.userid).subscribe((res)=>{console.log(res)},(err)=>{console.log(err)})

    
     //this.service.editUserInfo(this.userid,this.editProfileForm.value).subscribe((res)=>{console.log(res)},(err)=>{console.log(err)})
   }
   else
   {
    console.log("what the fuck")
   }
}
  constructor(private service:RegisterService,private userInfo:OrdersSpecificUserService, private authSerivce: AuthService,private userProfile : LoginService) { 
   //this.x=this.editProfileForm.setValue({name:"hadeeeeeeeeeeeeeeeeeer"})
  }

  ngOnInit(): void {
    let token = localStorage.getItem('token');
    this.authSerivce.authorizeUser({ token: token }).subscribe(
      (s) => {
       this.user=s;
        this.userid=s._id;
       // this.user.name = s.name;
      

        
        this.userProfile.updateImagePathForUserProfile(s);    
        this.userImage = s.userImage;
         console.log("image is", this.userImage )

         this.createFile(this.userImage); 

      },
      (err) => {
        console.log(err);
      }
    );

  }


  onFileSelected(evetObj) {
    this.selectedFile = evetObj.target.files[0];

    console.log(this.selectedFile);
  }


   async  createFile(imageURL){
    let response = await fetch(imageURL);
    let data = await response.blob();
    let metadata = {
      type: 'image/*'
    };
    
    
    this.selectedFile = new File([data], imageURL, metadata);
    // ... do something with the file or return it
  }

}


