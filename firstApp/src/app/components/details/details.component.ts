import { Component, OnInit } from '@angular/core';
import { ProductDeatilsService } from 'src/app/Services/product-deatils.service';
import { Product } from 'src/app/Model/product.Model';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductDataService } from 'src/app/Services/product.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { CartService } from 'src/app/Services/cart.service';
import { MessangerService } from 'src/app/Services/messanger.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css'],
})
export class DetailsComponent implements OnInit {
  id;
  userid;
  product;
  constructor(
    private authService: AuthService,
    private prodDetails: ProductDeatilsService,
    public route: ActivatedRoute,
    private productService: ProductDataService,
    private router: Router,
    private cartService: CartService,
    private msg: MessangerService
  ) {
    console.log('in details componentt');
    this.id = this.route.snapshot.paramMap.get('id');
    console.log(this.id);
    this.prodDetails.getProdctInfo(this.id).subscribe(
      (res) => {
        console.log('hadeeeer', res);
        this.product = res;
        this.productService.updateImagesPathForProduct(this.product);
      },
      (err) => {
        console.log(err);
      }
    );
  }

  ngOnInit(): void {
    let token = localStorage.getItem('token');
    this.authService.authorizeUser({ token: token }).subscribe(
      (s) => {
        this.userid = s._id;
      },
      (err) => {
        console.log(err);
      }
    );
  }

  prdRoute() {
    this.router.navigate(['products']);
  }
  addToCart() {
    this.cartService.addProductToCart(this.id, this.userid).subscribe(() => {
      this.msg.sendMsg(this.id);
    });
  }
}
