import { Component, OnInit, Input } from '@angular/core';
import { OrdersSpecificUserService } from 'src/app/Services/orders-specific-user.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { OrdersService } from 'src/app/Services/orders.service';
import {LoginService} from 'src/app/Services/login/login.service'
import { ProductDataService } from 'src/app/Services/product.service';



@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {

  OrdersTitle = 'My Orders';
  InfoTitle = 'Account Info';

  orders;
  user;
  userid;
  @Input() userImage;
  constructor(private service: OrdersSpecificUserService, private router: Router, private authSerivce: AuthService,private Cancelservice:OrdersService,private userProfile : LoginService ,private productService:ProductDataService ) { }

  ngOnInit(): void {

    let token = localStorage.getItem('token');
    this.authSerivce.authorizeUser({ token: token }).subscribe(
      (s) => {
      this.user=s;
        this.userid=s._id

        //this.userImage = "localhost:3000/"+s.userImage;
        this.userProfile.updateImagePathForUserProfile(s);
        
        this.userImage = s.userImage;

        this.service.getOrders(this.userid).subscribe((res) => {
         // this.orders.push(res);// = res
          this.orders = res;
        this.orders.forEach(element => {
          console.log(element.cart.products);
          this.productService.updateImagesPathForProduct(element.cart.products);
});


        //  console.log(this.orders)


         // this.productService.updateImagesPathForProduct(this.orders.cart.products); 
           
          }, (err) => { console.log(err) })
      
          this.service.getUserInfo(this.userid).subscribe((res) => { 
            


            this.user = res }, (err) => { console.log(err) })
        console.log('emaaaan test',s)
        console.log('user id', s._id);
      },
      (err) => {
        console.log(err);
      }
    );

    //  this.service.getOrders(this.userid).subscribe((res) => {
    // this.orders.push(res) 
    //    console.log(res)
    //  }, (err) => { console.log(err) })


  }
  testroute() {
    this.router.navigate(['editProfile']);
  }
  cancelOrder(id){
    this.Cancelservice.editStatus(id,{orderStatus:"cancelled"}).subscribe((res) => {
      alert("Your order has been cancelled");
      this.orders=res;
        console.log(res)
      }, (err) => { console.log(err) })
    }

  }

