import { Component, OnInit, Input } from '@angular/core';
import { Product } from 'src/app/Model/product.Model';
import { CartService } from 'src/app/Services/cart.service';
import {MessangerService} from 'src/app/Services/messanger.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ProductDeatilsService } from 'src/app/Services/product-deatils.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.css']
})
export class ProductItemComponent implements OnInit {

  @Input() productItem:Product;
  userid;
id;
  constructor(private msg: MessangerService, private router: Router,
    private cartService: CartService,private authService:AuthService,private prodDetails:ProductDeatilsService) { }

  ngOnInit(): void {
    let token = localStorage.getItem('token');
    this.authService.authorizeUser({ token: token }).subscribe(
      (s) => {
        this.userid=s._id
      },
      (err) => {
        console.log(err);
      })
  }

  
   handleAddToCart() {  
     (
       
       this.cartService.addProductToCart(this.productItem._id, this.userid)).subscribe(() => {
        this.msg.sendMsg(this.productItem)
    })
   
  }
  showDetails(){
    this.prodDetails.getProdctInfo(this.productItem._id).subscribe((res)=>{console.log(res)},(err)=>{console.log(err)})
  }
  routeDetails(id){
    this.id=this.productItem._id;
    this.router.navigate(['/details',this.id]);
  }

}
