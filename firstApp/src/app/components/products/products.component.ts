import { Component, OnInit, TemplateRef } from '@angular/core';
import { Product } from 'src/app/Model/product.Model';
import { ProductDataService } from 'src/app/Services/product.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MessangerService } from 'src/app/Services/messanger.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
})
export class ProductsComponent implements OnInit {
  public term: string;
  products$: Product[];
  closeResult;
  constructor(
    private productService: ProductDataService,
    private modalSerivce: NgbModal,
    private msg: MessangerService, 
    private router : Router
  ) {}

  ngOnInit() {
    this.msg.getToken().subscribe(
      (token) => {
        if (token || this.isAuthenticated()) this.loadProducts();
      },
      (err) => {
        alert('no token');
        this.msg.getModalContent().subscribe((content) => {
       
          this.modalSerivce
            .open(content, { size: 'lg' })
            .result.then((result) => {
              this.closeResult = `closed with ${result}`;
            });
        });
        
      }
    );
  }
  loadProducts() {
    this.productService.getProducts().subscribe((products) => {   
      this.products$ = products;
       this.productService.updateImagesPathForProduct(this.products$); 
    })
  }
  routeCreate(){
    this.router.navigate(['adminCreateProducts']);
  }

  isAuthenticated(): Boolean {
    let token = localStorage.getItem('token');
    if (token) return true;
    else return false;
  }
}
