import { Component, OnInit } from '@angular/core';
import { OrdersService } from 'src/app/Services/orders.service';

@Component({
  selector: 'app-orders-admin',
  templateUrl: './orders-admin.component.html',
  styleUrls: ['./orders-admin.component.css']
})
export class OrdersAdminComponent implements OnInit {
  orders;
  newStatus;
  id;
  constructor(private service:OrdersService) { }

  ngOnInit(): void {
  this.getOrders();
   
  }
  getOrders()
  {
    this.service.getOrders().subscribe((res) => {
      this.orders=res;
        console.log(res)
      }, (err) => { console.log(err) })

      this.service.editStatus(this.id,this.newStatus).subscribe((res) => {
        this.orders.push(res)
          console.log(res)
        }, (err) => { console.log(err) })

  }

  selectStatus(id:string,stat:string){
    //this.orders.orderStatus=stat;
  this.service.editStatus(id,{orderStatus:stat}).subscribe((res) => {
    console.log("idddd",id);
    console.log("new statuss",stat);
    this.orders=res;
    this.getOrders();
    // this.orders.orderStatus=res.orderStatus;
      console.log(res)
    }, (err) => { console.log(err) })
  }


}
