import { Component, OnInit } from '@angular/core';
import { MessangerService } from 'src/app/Services/messanger.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css'],
})
export class LogoutComponent implements OnInit {
  isLoggedIn: boolean = false;
  constructor(private messangerService: MessangerService , private router: Router) {}

  ngOnInit(): void {
    let token = localStorage.getItem('token');
    if (token) {
      this.isLoggedIn = true;
    }
  }

  logout() {
    console.log('logout hereeeeeeeeeeeeeeeeeee');
    this.messangerService.fireLogout();
    localStorage.removeItem('token');
    localStorage.clear();
    this.router.navigate(['/']);
  }
}
