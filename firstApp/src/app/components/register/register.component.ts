import { Component, OnInit, Output, OnDestroy } from '@angular/core';
import { RegisterService } from 'src/app/services/register/register.service';
import {
  FormGroup,
  FormControl,
  Validators,
  ReactiveFormsModule,
  FormsModule,
} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { renderFlagCheckIfStmt } from '@angular/compiler/src/render3/view/template';
import { EventEmitter } from 'events';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/auth/auth.service';
import { LoginService } from 'src/app/services/login/login.service';
import { MessangerService } from 'src/app/Services/messanger.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  img = `assets/images/1.jpg`;
  flag: boolean;
  notValidFlag: boolean;
  notValidPassFlag: boolean;
  userName: string ;
  email: string;
  password: string;
  confirmPassword: string;
  selectedFile: File;
  selection;
  @Output() myEvent = new EventEmitter();

  constructor(
    private reigsterService: RegisterService,
    private router: Router,
    private loginSerivce: LoginService,
    private msg: MessangerService
  ) {}

  ngOnInit(): void {}
  onFileSelected(evetObj) {
    this.selectedFile = evetObj.target.files[0];
    console.log(this.selectedFile);
  }
  signUp() {
    // console.log(this.myFrom.get('password'));
    // let userObj = {
    //   name: this.userName,
    //   email: this.email,
    //   password: this.password,
    //   file: this.selectedFile,
    //   gender: this.selection,
    //   // confirmPassword: this.confirmPassword,
    // };
    // this.myFrom.controls['image'].setValue(this.selectedFile)
    console.log(this.myFrom.controls['email'].value);
    let user = {
      name: this.myFrom.controls['name'].value,
      email: this.myFrom.controls['email'].value,
      password: this.myFrom.controls['password'].value,
      file: this.selectedFile,
      gender: this.selection,
    };
    // console.log(this.selection);

    // this.myFrom.controls['name'].setValue(this.userName);
    // this.myFrom.controls['password'].setValue(this.password);
    // this.myFrom.controls['email'].setValue(this.email);
    this.reigsterService.createUser(user).subscribe(
      (res) => {
        console.log(res);
        let resString = JSON.stringify(res);
        // let resJson = JSON.parse(resString);
        let resJson = JSON.parse(resString);
        // this.myEvent.emit(resJson.name);
        console.log(resJson);
        console.log(resJson._id);
        console.log(resJson.name);
        console.log(resJson.email);
        console.log(resJson.gender);

        // this.msg.sendUserName(resJson.name);
        this.msg.sendUserName(resJson);

        ///for authentication

        this.loginSerivce
          .addUser({ email: user.email, password: user.password })
          .subscribe((res) => {
            localStorage.setItem('token', res.token);
            this.msg.sendToken({token :res.token});
            console.log(res);
          });
        this.router.navigate(['products']);
        //this.loginSerivce.updateImagePathForUserProfile(userObj)

      },
      (err) => {
        console.log('create user erro', err.error);
        let v: String;
        let emailValidation: string;
        let passValidation: string;

        v = 'user is already registered';
        emailValidation = "'email' must be a valid email";
        passValidation = "'password' is required";
        var result = err.error.localeCompare(v);
        console.log(err.error);
        let notValidEmail = err.error.localeCompare(emailValidation);
        let notValidPass = err.error.localeCompare(passValidation);
        if (result === 1) this.flag = true;
        else if (notValidPass === 1) {
          this.notValidPassFlag = true;
          notValidPass = 0;
        } else if (notValidEmail === 1) {
          this.notValidFlag = true;
          console.log(notValidEmail);
          notValidEmail = 0;
        }
      }
    );
    // console.log(this.myFrom.value);
  }

  myFrom = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(8)]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(5),
      Validators.max(20),
    ]),
    email: new FormControl('', [Validators.required, Validators.email]),
    gender: new FormControl('', [Validators.required]),
    image: new FormControl(''),
  });

  get nameStatus() {
    return this.myFrom.controls.name.valid;
  }

  // registerForm = new FormGroup({
  //   user: new FormControl('', [Validators.required]),
  //   // email: new FormControl(' ', Validators.email),
  //   // password: new FormControl(' ', [
  //   //   Validators.required,
  //   //   Validators.minLength(7),
  //   //   Validators.maxLength(20),
  //   // ]),
  //   // phone: new FormControl('', Validators.minLength(11)),
  // });

  // get nameStatus() {

  //   return this.registerForm.controls.user.valid;
  //   // return this.registerForm.get('name');
  // }
}
