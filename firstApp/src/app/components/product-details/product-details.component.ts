import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductDataService } from 'src/app/Services/product.service';
import { productsUrl } from 'src/app/config/api';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css'],
})
export class ProductDetailsComponent implements OnInit, OnDestroy {
  id;
  subscriber;
  // product = {
  //   _id: String,
  //   title: String,
  //   description: String,
  //   price: Number,
  //   quantity: Number,
  //   file: String,
  // };

  product;
  selectedFile;
  title;
  constructor(
    private myActivatedRoute: ActivatedRoute,
    private productService: ProductDataService,
    private router : Router
  ) {
    this.id = this.myActivatedRoute.snapshot.params['id'];
    console.log(this.id);
  }
  ngOnDestroy(): void {
    this.subscriber.unsubscribe();
  }

  ngOnInit(): void {
    this.getProduct();
  }

  getProduct() {
    this.subscriber = this.productService.getProductById(this.id).subscribe(
      (product: any) => {
        // if (product)
        // this.productService.updateProduct(product);
        this.product = product;
        console.log(product.hasOwnProperty('title'));
        var x = JSON.parse(JSON.stringify(product));

        console.log(x.title);
      },
      (err) => {
        console.log(err);
      }
    );
  }
  onFileSelected(data) {
    this.selectedFile = data.target.files[0];
    this.product.file = this.selectedFile;
    console.log(data.target.files[0]);
  }

  saveChanges() {
    this.productService.updateProduct(this.product).subscribe(
      (product) => {
        console.log(product);
        alert('your product updated successfully');
        this.router.navigate(['products'])
      },
      (err) => {
        console.log(err.message.details);
      }
    );
  }
}
